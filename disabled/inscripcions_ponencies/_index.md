---
title: "Proposar una ponència"
date: 2018-05-29T11:29:49+02:00
draft: false
weight: 20
---

El format escollit per a les ponències serà la xerrada llampec, suficient per conèixer la idea de l'experiència i, al mateix temps, una quantitat generosa de propostes. Hi haurà una versió curta, 5 minuts, una estesa, de 15 minuts, i les reflexions finals les construirem a la sobretaula que farem a un restaurant per concretar.

A mesura que anem rebent i confirmant ponències les anirem afegint al programa de la Jornada. La data limit per entregar propostes és el **17 de Juny**.

Qualsevol dubte pots escriure un email a jornades@obrimelcodi.cat
