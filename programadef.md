09:40 - 10:00        BLOC A: Ponències curtes de 5 minuts

​              

BLOC B: 3        ponències + preguntes

10:00 - 10:15        G-assist - Programari lliure de gestió d'alumnes i professors

10:15 - 10:30 Tux        vol entrar a l'institut  i no tenim informàtics ni enginyers.        Què puc fer, doctor?

10:30 - 10:45        Labdoo: Etiqueta un portàtil, estén l'educació per tot el món.

BLOC C: 3        ponències + preguntes

11:00 - 11:15        IsardVDI, entorn de escriptoris virtuals per educació

11:15 - 11:30        KDE/Plasma: creant l'escriptori Linux més lleuger i potent

11:30 - 11:45        Funcionalitats i novetats del projecte Linkat

Descans

BLOC D: Ponències        curtes de 5 minuts

​              

BLOC E: 3        ponències + preguntes              

12:30 - 12:45        Snap! Arcade

12:45 - 13:00        CommonsCloud: reapropriem-nos del núvol

13:00 - 13:15        Asciidoc, Git i GitLab com entorn virtual d'aprenentatge

BLOC E: 2        ponències + preguntes

13:15 - 13:30        Quan les dades són públiques, però no obertes: web scraping

13:30 - 13:45        Fotomatón DiY

13:45 - 14:00        Preguntes i clausura 



# Snap! Arcade

#### Ponent: Chris Fanning

***Una máquina arcade que corre proyectos programados en Snap! que sirve como aparador que incentiva la programación de proyectos hechos por alumnos ya que se suben a la máquina para que todxs los puedan disfrutar!***

*https://snaparcade.cat*
Vivim en un món cada vegada més digital i els canvis se succeeixen tan ràpid que no tenim gairebé temps per aturar-nos a reflexionar sobre totes aquestes màquines i programes que ens envolten.

La creació d'aquesta màquina recreativa, a més de fomentar la col·laboració entre diversos agents, pretén ser un aparador que desperti l'interès de la ciutadania en relació a les tecnologies lliures, ètiques i distribuïdes.

La construcción de la máquina incluye:
Hardware
- Diseño 3D.
- Impresión 3D
- Cortadora CNC
- Electrónica (botones, joystick, etc)
Software
- Controlador arduino
- Systema opertaivo Debian.
- Programación de una API. backend python, frontend vuejs
- Bash
- HTML, CSS
- Git

Si bé l'objectiu de la primera fase del projecte és la pròpia construcció de la màquina, la segona fase té un doble objectiu:

- Crear contingut digital per la màquina amb eines lliures, fomentant així l'aprenentatge de nous conceptes relacionats amb les tecnologies creatives i el pensament computacional.
- Apropar conceptes i eines relacionades amb la sobirania tecnològica per tal d'apoderar el veïnat en la reflexió i l'us d'infraestructura i programari lliures, en la comprensió d'una realitat canviant on afloren noves competències i oficis


#Labdoo

#### Ponent: Franc Rosselló Ribes (INS Eugeni d'Ors Vilafranca)

***Etiqueta un portàtil, estén l'educació per tot el món.***

Labdoo és una xarxa solidaria mundial que restaura portàtils, instal·lant edubuntu i programari educatiu lliure porta l'educació arreu del món.

En el meu centre fem una pràctica en el mòdul de SOM, conjunta amb MME, on etiqueten, documenten, reparen si s'escau - i podem - el portàtil, instal·lem edubuntu i el programa necessari. 
A través de la xarxa social labdoo.org viatgers porten els portàtils a les EdooVillages on és necessiten els portàtils, des de Barcelona a Vanuatu.


# Tux vol entrar a l'institut  i no tenim informàtics ni enginyers. Què puc fer, doctor?

**Ponent: Francisco Javier Teruelo de Luis** **(INS Terra Roja)**

***Experiències de llarg recorregut de transformació d'un centre a línux***	
		
Difussió de l'experiència de més de deu anys com a Coordinador Informàtic d'un centre transformat a Línux i mantingut dintre del programa oficial del Departament d'Ensenyament -Projecte Linkat-, amb la concurrència, en el mateix temps, del desenvolupament del projecte 1x1 i la digitalització de les aules.



# Funcionalitats i novetats del projecte Linkat

**Pablo Vigo (TicxCat al Dept. Ensenyament)**

***El projecte Linkat vol ser el referent del programari lliure a les escoles i per això continuem desenvolupant noves funcionalitats per ajudar el dia a dia dels centres educatius.***

Les principals funcionalitats del projecte són les següents:
- Nova versió Linkat edu 18.04 basada en Ubuntu LTS: Nou servidor de cenrtre amb més funcionalitats i clients amb un gran recull d'aplicacions educatives.

- Perfils d'usuari: Escriptoris personalitzats per a infantil, primària, secundària, classe de robòtica, ràdio...

- Escriptoris remots per HTML5: L'escriptori Linkat des de qualsevol navegador web.

- Congelador d'escriptoris: Manté l'escriptori sempre a punt.

- Linkat lleugera: Aprofita ordinadors de baixes prestacions.
   LTSP: Un aula d'informàtica de baix cost.

   ​	


# Asciidoc, Git i GitLab com entorn virtual d'aprenentatge

**Joan Queralt Molina (Institut Castellet)** 

***S'explicarà com podem utilitzar la combinació del GitLab, el Git, i l'Asciidoc (entre d'altres tecnologies) com un entorn d'aprenentatge que ens faciliti la creació col·laborativa de materials lliures, i l'entrega i correcció d'exercicis.***

El Git és un sistema de control de versions que s'utilitza especialment per al codi font de programari, però que ens permet treballar amb qualsevol tipus de fitxer, i que és especialment indicat per fitxers de text pla.

L'Asciidoc és un format d'etiquetes lleuger que ens facilita la creació de documentació utilitzant qualsevol editor de textos i fitxers de text pla.

El Gitlab és un portal web que amplia les funcionalitats del Git de múltiples formes, entre elles la gestió de tasques, la visualització de fitxers Asciidoc, i l'opció d'afegir comentaris sobre les diferents versions d'un document.

En aquesta ponència s'explicarà com podem utilitzar la combinació d'aquestes tecnologies com un entorn d'aprenentatge que ens faciliti la creació col·laborativa de materials lliures, l'entrega i correcció d'exercicis, i el desenvolupament de projectes per part de l'alumnat amb el corresponent seguiment del professor.

Aquesta metodologia és especialment indicada pels cicles formatius d'informàtica de desenvolupament d'aplicacions (multiplataforma o web), però pot resultar pràctica també en altres àmbits.

Entre d'altres avantatges, aquest sistema ens permet la correcció i ampliació dels materials de classe de forma immediata, i la possibilitat de comentar el codi dels nostres alumnes línia a línia directament des del navegador web.

# Instal·lació automatitzada de Debian per aules d'informàtica

**Joan Queralt Molina (Institut Castellet)** 

***En aquesta ponència s'explicarà l'experiència en l'ús d'un instal·lador automatitzat de Debian per les aules de cicles formatius d'informàtica, i es mostraran els passos bàsics per adaptar-lo a les necessitats pròpies de cada centre.***

L'instal·lador de Debian ofereix la possibilitat d'automatitzar una instal·lació, indicant en un fitxer de configuració les respostes a les preguntes que es fan habitualment durant el procés. A més, permet l'execució d'un script addicional que deixi el sistema exactament com el necessitem (creació d'usuaris, configuració de repositoris no oficials, instal·lació de programari addicional, etc).

La instal·lació automatitzada ofereix una bona alternativa a les clonacions d'ordinadors: no cal guardar imatges mestre, no es depèn de la xarxa per fer les instal·lacions, i no cal preocupar-se de les variants de maquinari sobre les quals cal fer la instal·lació. Simplement es copiarà l'instal·lador automàtic a les memòries USB necessàries i s'arrencaran els ordinador a instal·lar utilitzant aquestes memòries. En aproximadament mitja hora l'aula estarà preparada.

En aquesta ponència s'explicarà l'experiència en l'ús d'un instal·lador automatitzat per les aules de cicles formatius d'informàtica, i es mostraran els passos bàsics per adaptar-lo a les necessitats pròpies de cada centre.

El procés està documentat a https://gitlab.com/joanq/insdebauto.



G-assist - Programari lliure de gestió d'alumnes i professors

GEISoft és un programari lliure desenvolupat per professors del DE per gestionar assistència a ESO/BAT/CCFF LOE i altres aspectes de funcionament de centre com la disciplina, les guàrdies, absències de professorat,...



G-assist és un programari fruit del desenvolupament de professorat del Departament d'informàtica de l'INS Nicolau Copèrnic. Va néixer ja fa 7 anys i l'utilitzen més de 50 centres arreu de Catalunya. Dóna suport de forma nativa als CCFF LOE i la seva complexitat d'UFs i dades d'inici i de fi. 