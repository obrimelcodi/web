---
title: "Col·laborar"
date: 2018-05-29T11:29:49+02:00
draft: false
weight: 120
---
El grup de treball que estem organitzant aquestes jornades està format per uns quants professors i altres persones relacionades amb el món del software lliure. Hi ha feina i ens agradaria que més persones formessin part d'aquest equip de treball. Si vols col·laborar envia un email a jornades@obrimelcodi.cat
