---
title: "Horari"
date: 2018-05-29T11:29:49+02:00
draft: false
layout: prova
---

## Horari de la jornada

### Dimecres 27 de juny de 2018

## Ponències

09:00 - 09:30 Acollida
09:30 - 09:40 Benvinguda

### BLOC A

Ponències de 5 minuts

| HORA         | TÍTOL                                                        | PONENT              | ORGANITZACIÓ                      |
| ------------ | ------------------------------------------------------------ | ------------------- | --------------------------------- |
| 9:40 - 9:47  | Documentar amb markdown i publicar-ho a una web              | Néfix               | INS Escola del Treball, Barcelona |
| 9:47 - 9:53  | PXE Cloud                                                    | Jorge Pastor        | INS Escola del Treball, Barcelona |
| 9:53 - 10:00 | Instal·lació automatitzada de Debian per aules d'informàtica | Joan Queralt Molina | Institut Castellet                |

### BLOC B

Ponències de 15 minuts i preguntes

| HORA  | TÍTOL                                                        | PONENT                           | ORGANITZACIÓ                  |
| ----- | ------------------------------------------------------------ | -------------------------------- | ----------------------------- |
| 10:00 | G-assist - Programari lliure de gestió d'alumnes i professors | Victor Manuel Lino Martínez      | GEISoft, INS Nicolau Copèrnic |
| 10:15 | Funcionalitats i novetats del projecte Linkat              | Pablo Vigo                 | TicxCat al Dept. Ensenyament      |
| 10:30 | Labdoo: Etiqueta un portàtil, estén l'educació per tot el món. | Franc Rosselló Ribes             | INS Eugeni d'Ors Vilafranca   |

### BLOC C

Ponències de 15 minuts i preguntes

| HORA  | TÍTOL                                                      | PONENT                     | ORGANITZACIÓ                      |
| ----- | ---------------------------------------------------------- | -------------------------- | --------------------------------- |
| 11:00 | django-aula                                                | Daniel Herrera             |                                   |
| 11:15 | KDE/Plasma: creant l'escriptori Linux més lleuger i potent | Aleix Pol                  | KDE                               |
| 11:30 | Tux vol entrar a l'institut  i no tenim informàtics ni enginyers. Què puc fer, doctor? | Francisco Javier Teruelo de Luis | INS Terra Roja                |


## DESCANS

### BLOC D

Ponències de 5 minuts

| HORA  | TÍTOL                               | PONENT              | ORGANITZACIÓ                      |
| ----- | ----------------------------------- | ------------------- | --------------------------------- |
| 12:15 | Fotomatón DiY                       | Josep Maria Viñolas | INS Escola del Treball, Barcelona |
| 12:22 | KDE Edu                             | Aleix Pol           | KDE                               |
|       |                                     |                     |                                   |

### BLOC E

Ponències de 15 minuts i preguntes

| HORA  | TÍTOL                                                    | PONENT              | ORGANITZACIÓ             |
| ----- | -------------------------------------------------------- | ------------------- | ------------------------ |
| 12:30 | Snap! Arcade                                             | Chris Fanning       | https://snaparcade.cat   |
| 12:45 | CommonsCloud: reapropriem-nos del núvol                  | Wouter Tebbens      | Free Knowledge Institute |
| 13:00 | Asciidoc, Git i GitLab com entorn virtual d'aprenentatge | Joan Queralt Molina | Institut Castellet       |

### BLOC F

Ponències i preguntes

| HORA  | TÍTOL                                                       | PONENT                    | ORGANITZACIÓ           |
| ----- | ----------------------------------------------------------- | ------------------------- | ---------------------- |
| 13:15 | Quan les dades són públiques, però no obertes: web scraping | Mònica Ramírez Arceda     | USTEC·STEs (IAC)       |
| 13:30 | IsardVDI, entorn de escriptoris virtuals per educació      | Alberto Larraz Dalmases | INS Escola del Treball, Barcelona |
| 13:45 | Preguntes i clausura                                        |                           |                        |



## 14:00 Dinar (12 euros cal reservar al fer l'inscripció)
