---
title: "Ponències"
date: 2018-05-29T11:29:49+02:00
draft: false
#layout: prova
---

## Ponències

#### 9:40 - 9:47  Com muntar la nostra pròpia web de documentació

La documentació és una part bàsica de qualsevol projecte, configuració i programa. Molts cops, per manca de temps o perquè no ve de gust, moltes coses acaben sense documentar. També és molt important poder accedir a la nostra documentació des de qualsevol lloc i en qualsevol moment. En aquesta ponència s'explicarà com muntar una web de documentació que no ens suposi cap mena d'esforç extra ni que ens faci perdre el temps.



- PONENT: Néfix Estrada
- ORGANITZACIÓ: INS Escola del Treball, Barcelona

#### 9:47 - 9:53  PXE Cloud

PxeCloud és un projecte que va sortir amb la finalització de curs GM, la seva finalitat és facilitar la feina a l'administrador, desplegant rapidament els serveis necessaris per connectar a un servidor iPXE extern, i així poder administrar diversos centres amb una aplicació que genera els menús iPXE dinàmicament

- PONENT: Jorge Pastor
- ORGANITZACIÓ: INS Escola del Treball, Barcelona

#### 9:53 - 10:00  Instal·lació automatitzada de Debian per aules d'informàtica

En aquesta ponència s'explicarà l'experiència en l'ús d'un instal·lador automatitzat de Debian per les aules de cicles formatius d'informàtica, i es mostraran els passos bàsics per adaptar-lo a les necessitats pròpies de cada centre. 

L'instal·lador de Debian ofereix la possibilitat d'automatitzar una instal·lació, indicant en un fitxer de configuració les respostes a les preguntes que es fan habitualment durant el procés. A més, permet l'execució d'un script addicional que deixi el sistema exactament com el necessitem (creació d'usuaris, configuració de repositoris no oficials, instal·lació de programari addicional, etc).

La instal·lació automatitzada ofereix una bona alternativa a les clonacions d'ordinadors: no cal guardar imatges mestre, no es depèn de la xarxa per fer les instal·lacions, i no cal preocupar-se de les variants de maquinari sobre les quals cal fer la instal·lació. Simplement es copiarà l'instal·lador automàtic a les memòries USB necessàries i s'arrencaran els ordinador a instal·lar utilitzant aquestes memòries. En aproximadament mitja hora l'aula estarà preparada.

En aquesta ponència s'explicarà l'experiència en l'ús d'un instal·lador automatitzat per les aules de cicles formatius d'informàtica, i es mostraran els passos bàsics per adaptar-lo a les necessitats pròpies de cada centre.

El procés està documentat a https://gitlab.com/joanq/insdebauto.

- PONENT: Joan Queralt Molina
- ORGANITZACIÓ: Institut Castellet

#### 10:00  G-assist - Programari lliure de gestió d'alumnes i professors

GEISoft és un programari lliure desenvolupat per professors del DE per gestionar assistència a ESO/BAT/CCFF LOE i altres aspectes de funcionament de centre com la disciplina, les guàrdies, absències de professorat,... G-assist és un programari fruit del desenvolupament de professorat del Departament d'informàtica de l'INS Nicolau Copèrnic. Va néixer ja fa 7 anys i l'utilitzen més de 50 centres arreu de Catalunya. Dóna suport de forma nativa als CCFF LOE i la seva complexitat d'UFs i dades d'inici i de fi. 
Creix amb els suggeriments i aportacions dels centres.
Consta d'un bloc bàsic que es pot descarregar lliurement des de la web del projecte i gestiona l'assistència dels alumnes, i altres blocs addicionals per al control de la disciplina, les guàrdies, les sortides, .....

- PONENT: Victor Manuel Lino Martínez
- ORGANITZACIÓ: GEISoft, INS Nicolau Copèrnic

#### 10:15 Funcionalitats i novetats del projecte Linkat

El projecte Linkat vol ser el referent del programari lliure a les escoles i per això continuem desenvolupant noves funcionalitats per ajudar el dia a dia dels centres educatius.

- Les principals funcionalitats del projecte són les següents:
  - Nova versió Linkat edu 18.04 basada en Ubuntu LTS: Nou servidor de cenrtre amb més funcionalitats i clients amb un gran recull d'aplicacions educatives.
  - Perfils d'usuari: Escriptoris personalitzats per a infantil, primària, secundària, classe de robòtica, ràdio...
  - Escriptoris remots per HTML5: L'escriptori Linkat des de qualsevol navegador web.
  - Congelador d'escriptoris: Manté l'escriptori sempre a punt.
  - Linkat lleugera: Aprofita ordinadors de baixes prestacions.
  - LTSP: Un aula d'informàtica de baix cost.

- PONENT: Pablo Vigo

- ORGANITZACIÓ: TicxCat al Dept. Ensenyament

#### 10:30  Labdoo: Etiqueta un portàtil, estén l'educació per tot el món.

Labdoo és una xarxa solidaria mundial que restaura portàtils, instal·lant edubuntu i programari educatiu lliure porta l'educació arreu del món.
En el meu centre fem una pràctica en el mòdul de SOM, conjunta amb MME, on etiqueten, documenten, reparen si s'escau - i podem - el portàtil, instal·lem edubuntu i el programa necessari. 
A través de la xarxa social labdoo.org viatgers porten els portàtils a les EdooVillages on és necessiten els portàtils, des de Barcelona a Vanuatu.

- PONENT: Franc Rosselló Ribes
- ORGANITZACIÓ: INS Eugeni d'Ors Vilafranca


#### 11:00 django-aula (djau)

*django-aula* (*djau*) és un projecte de codi obert per a gestió de presència, incidències i més per a instituts, escoles i acadèmies.
Està escrit en *Python* (*django*) per la comunitat i dissenyat a partir de tres premisses: menys feina, estalvi temps, millor gestió i millor rendiment acadèmic.
Explicarem breument com ha estat la creació de programari lliure a partir de programari lliure, la nostra experiència com a comunitat djau i els plans de futur.


- PONENT: Daniel Herrera, Manel Pérez, Xavier Sala
- ORGANITZACIÓ: Ins Cendrassos, Ins Sa Palomera

#### 11:15  KDE/Plasma: creant l'escriptori Linux més lleuger i potent

Parlarem de com s'aprofita la reducció de consum i us mostrarem com es materialitza a la pràctica en crear entorns de treball.

- PONENT: Aleix Pol
- ORGANITZACIÓ: KDE

#### 11:30  Tux vol entrar a l'institut  i no tenim informàtics ni enginyers. Què puc fer, doctor?

Difusió de l'experiència de més de deu anys com a Coordinador Informàtic d'un centre transformat a Línux i mantingut dintre del programa oficial del Departament d'Ensenyament -Projecte Linkat-, amb la concurrència, en el mateix temps, del desenvolupament del projecte 1x1 i la digitalització de les aules.

- PONENT: Francisco Javier Teruelo de Luis
- ORGANITZACIÓ: INS Terra Roja

#### 12:15  Fotomatón DiY

A l'escola del treball tenim múltiples aplicacions de gestió pròpies que fan ús de la fotografia de professorat i alumnat. Vam crear un fotomatón automàtic amb reconeixement d'imatge amb OpenCV que actua sobre la posició de la càmera i el flash de manera automàtica. També puja la imatge final a un programari de gestió d'orles per facilitar la feina de classificació automàtica a partir del NIF de l'usuari.

- PONENT: Josep Maria Viñolas Auquer

- ORGANITZACIÓ:  INS Escola del Treball

#### 12:22  KDE Edu

Veurem les diferents aplicacions de KDE Edu. Quines hi ha, de què serveixen i on trobar-les.

- PONENT: Aleix Pol

- ORGANITZACIÓ: KDE

#### 12:30  Snap! Arcade

https://snaparcade.cat
Vivim en un món cada vegada més digital i els canvis se succeeixen tan ràpid que no tenim gairebé temps per aturar-nos a reflexionar sobre totes aquestes màquines i programes que ens envolten.

La creació d'aquesta màquina recreativa, a més de fomentar la col·laboració entre diversos agents, pretén ser un aparador que desperti l'interès de la ciutadania en relació a les tecnologies lliures, ètiques i distribuïdes.

La construcció de la màquina inclou:
Hardware
- Disseny 3D.
- Impressió 3D
- Talladora CNC
- Electrònica (botons, joystick, etc)
  Software
- Controlador arduino
- Systema opertaiu Debian.
- Programació d'una API, backend python, frontend vuejs
- Bash
- HTML, CSS
- Git

Si bé l'objectiu de la primera fase del projecte és la pròpia construcció de la màquina, la segona fase té un doble objectiu:

- Crear contingut digital per la màquina amb eines lliures, fomentant així l'aprenentatge de nous conceptes relacionats amb les tecnologies creatives i el pensament computacional.
- Apropar conceptes i eines relacionades amb la sobirania tecnològica per tal d'apoderar el veïnat en la reflexió i l'ús d'infraestructura i programari lliures, en la comprensió d'una realitat canviant on afloren noves competències i oficis

- PONENT: Chris Fanning

- ORGANITZACIÓ: https://snaparcade.cat

#### 12:45  CommonsCloud: reapropriem-nos del núvol

Ens preocupa la creixent dependencia de grans plataformes digitals en mans de grandes corporacions. Tenim les eines del programari lliure, però cal col·lectivitzar l’esforç per fer-ho sostenible. CommonsCloud neix amb la voluntat de cooperativitzar els núvols i cultivar les eines i coneixements per fer-ne ús, de forma replicable, descentralitzat i federable.  

CommonsCloud neix com una aliança de 9 entitats a Barcelona compromeses amb la sobirania tecnològica que es van unir durant el 2017 per triar les millors eines lliures existents i desenvolupar una solució usable i sostenible. A la primavera del 2017 van fer la primera campanya de micromecentage al Goteo Conjuntament amb suport de l’Ajuntament de Barcelona. Així van posar en marxa els primers serveis, de Oficina, Projecte i l’Agora. Ara cal obrir l’horitzont per extendre el seu ús, la millora dels serveis i l’ampliació de les aplicacions segons les necessitats de les usuàries i col·lectius.

[http://commonscloud.coop](http://commonscloud.coop/)

- PONENT: Wouter Tebbens
- ORGANITZACIÓ: Free Knowledge Institute

#### 13:00  Asciidoc, Git i GitLab com entorn virtual d'aprenentatge

S'explicarà com podem utilitzar la combinació del GitLab, el Git, i l'Asciidoc (entre d'altres tecnologies) com un entorn d'aprenentatge que ens faciliti la creació col·laborativa de materials lliures, i l'entrega i correcció d'exercicis.","El Git és un sistema de control de versions que s'utilitza especialment per al codi font de programari, però que ens permet treballar amb qualsevol tipus de fitxer, i que és especialment indicat per fitxers de text pla.

L'Asciidoc és un format d'etiquetes lleuger que ens facilita la creació de documentació utilitzant qualsevol editor de textos i fitxers de text pla.

El Gitlab és un portal web que amplia les funcionalitats del Git de múltiples formes, entre elles la gestió de tasques, la visualització de fitxers Asciidoc, i l'opció d'afegir comentaris sobre les diferents versions d'un document.

En aquesta ponència s'explicarà com podem utilitzar la combinació d'aquestes tecnologies com un entorn d'aprenentatge que ens faciliti la creació col·laborativa de materials lliures, l'entrega i correcció d'exercicis, i el desenvolupament de projectes per part de l'alumnat amb el corresponent seguiment del professor.

Aquesta metodologia és especialment indicada pels cicles formatius d'informàtica de desenvolupament d'aplicacions (multiplataforma o web), però pot resultar pràctica també en altres àmbits.

Entre altres avantatges, aquest sistema ens permet la correcció i ampliació dels materials de classe de forma immediata, i la possibilitat de comentar el codi dels nostres alumnes línia a línia directament des del navegador web.

- PONENT: Joan Queralt Molina

- ORGANITZACIÓ: Institut Castellet

#### 13:15  Quan les dades són públiques, però no obertes: web scraping

El Departament d'Ensenyament ens ofereix multitud de dades que poden ser d'interès per als docents (plantilles, centres, números de borsa del professorat interí...). Moltes d'aquestes dades són de lliure accés per a qualsevol persona però el format en el qual ens ofereixen aquestes dades és difícil de manipular informàticament. 

En aquesta xerrada es posarà de manifest la dificultat que tenim els docents per tal de poder analitzar moltes de les dades que ens dóna el departament. Molts cops estan disseminades per diferents webs, tenen errors, són documents en format PDF o HTML pensats per ser llegits per humans i no per màquines. Es detallaran algunes de les eines que es poden fer servir per fer web scraping i així poder analitzar aquestes dades i treure'n conclusions.

- PONENT: Mònica Ramírez Arceda
- ORGANITZACIÓ: USTEC·STEs (IAC)

#### 13:30  IsardVDI, entorn de escriptoris virtuals per educació  

Els coordinadors d'informàtica a l'INS Escola del Treball de Barcelona van iniciar ja va 5 anys un projecte de codi obert que permeti de manera àgil i simple gestionar el desplegament de màquines virtuals amb KVM/qemu com  a hypervisors.

En aquesta presentació explicarem l'experiència viscuda a l'escola i la visió general del programari i les seves avantatges a l'aula.

- PONENT: Alberto Larraz Dalmases
- ORGANITZACIÓ: INS Escola del Treball, Barcelona
