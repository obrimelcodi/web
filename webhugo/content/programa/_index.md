---
title: "Programa"
date: 2018-05-29T11:29:49+02:00
draft: false
weight: 30
---
# Informació del programa de les jornades

### 09:00 Acollida
### 09:30 Benvinguda
#### 9:40  Com muntar la nostra pròpia web de documentació
#### 9:47  PXE Cloud
#### 9:53  Instal·lació automatitzada de Debian per aules d'informàtica
#### 10:00 G-assist - Programari lliure de gestió d'alumnes i professors
#### 10:15 Funcionalitats i novetats del projecte Linkat
#### 10:30 Labdoo: Etiqueta un portàtil, estén l'educació per tot el món.
#### 11:00 django-aula
#### 11:15 KDE/Plasma: creant l'escriptori Linux més lleuger i potent
#### 11:30 Tux vol entrar a l'institut  i no tenim informàtics ni enginyers. Què puc fer, doctor?
### 11:45 Descans
#### 12:15 Fotomatón DiY
#### 12:22 KDE Edu
#### 12:30 Snap! Arcade
#### 12:45 CommonsCloud: reapropriem-nos del núvol
#### 13:00 Asciidoc, Git i GitLab com entorn virtual d'aprenentatge
#### 13:15 Quan les dades són públiques, però no obertes: web scraping
#### 13:30 IsardVDI, entorn de escriptoris virtuals per educació  
### 13:45 Preguntes i clausura
### 14:00 Dinar (12 euros cal reservar al fer l'inscripció)
