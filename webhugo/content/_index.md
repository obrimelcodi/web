![](logos/obrimelcoco_50px.png)

## 1a Jornada de Programari Lliure als Centres Educatius

#### 27 de Juny de 2018 a l'Escola del Treball de Barcelona

Som molts, cada vegada més, els educadors que triem sempre que podem el programari lliure pels valors transversals que aporta en l'entorn del coneixement.

L'objectiu de la trobada és compartir diferents experiències i iniciatives relacionades amb el programari lliure orientades a l'educació. Volem reunir a persones de diferents centres educatius i organitzacions que estan interessades a promoure les tecnologies lliures a l'àmbit educatiu. L'esdeveniment és gratuït i et pots inscriure online.

- Formulari d'inscripció: **[Inscripcions](https://obrimelcodi.cat/inscripcio/)**

Organitza: Escola del Treball de Barcelona 

![](images/08013275_ESCOLA_DEL_TREBALL.jpg)



#### Qui Som i motivació

La idea de fer una jornada surt d'uns quants professors i desenvolupadors que volem promoure el software lliure als centres educatius. La majoria tenim relació amb l'Escola del Treball, on des de fa més de 15 anys hi ha una aposta clara per intentar fer servir linux i software lliure tant a la docència dels cicles formatius d'informàtica com per gestionar les infraestructures informàtiques del centre. D'altres es van afegint a la proposta que s'enriqueix i es desenvolupa amb un esperit obert i col·laboratiu. 

Creiem que cal donar un impuls a les iniciatives que promouen, desenvolupen i fan servir software lliure, tant per la docència com per dotar d'eines lliures als centres educatius. Pensem que cal trobar-nos, que cal fer xarxa i donar visibilitat a totes les persones que creiem que cal estendre la cultura del programari lliure a l'hàbitat educatiu. 

L'objectiu principal d'aquesta primera jornada és conèixer-nos, intercanviar experiències, aprendre i compartir moltes "mogudes" on es viu el software lliure. El format de xerrades curtes volem que ajudi a poder donar un ventall d'iniciatives i projectes, i el dinar posterior també està pensat per tenir un espai més informal on compartir. 



#### Programa definitiu

### 09:00 Acollida
### 09:30 Benvinguda
#### 9:40  Com muntar la nostra pròpia web de documentació
#### 9:47  PXE Cloud
#### 9:53  Instal·lació automatitzada de Debian per aules d'informàtica
#### 10:00 G-assist - Programari lliure de gestió d'alumnes i professors
#### 10:15 Funcionalitats i novetats del projecte Linkat
#### 10:30 Labdoo: Etiqueta un portàtil, estén l'educació per tot el món.
#### 11:00 django-aula
#### 11:15 KDE/Plasma: creant l'escriptori Linux més lleuger i potent
#### 11:30 Tux vol entrar a l'institut  i no tenim informàtics ni enginyers. Què puc fer, doctor?
### 11:45 Descans
#### 12:15 Fotomatón DiY
#### 12:22 KDE Edu
#### 12:30 Snap! Arcade
#### 12:45 CommonsCloud: reapropriem-nos del núvol
#### 13:00 Asciidoc, Git i GitLab com entorn virtual d'aprenentatge
#### 13:15 Quan les dades són públiques, però no obertes: web scraping
#### 13:30 IsardVDI, entorn de escriptoris virtuals per educació  
### 13:45 Preguntes i clausura
### 14:00 Dinar (12 euros cal reservar al fer l'inscripció)

Més informació:
- **[Horari](https://obrimelcodi.cat/programa/horari/)** 
- **[Ponències](https://obrimelcodi.cat/programa/ponencies/)** 
