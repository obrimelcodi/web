// Requires
const db = require('./db')

// Create the needed tables in the DB
const inscripcionsSql = `CREATE TABLE IF NOT EXISTS inscripcions (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    surnames TEXT NOT NULL,
    email TEXT NOT NULL,
    organization TEXT DEFAULT NULL,
    email_updates INTEGER DEFAULT 0 NOT NULL,
    lunch INTEGER DEFAULT 0 NOT NULL,
    token TEXT
  )`
db.run(inscripcionsSql, (err) => {
  if (err) {
    console.log(`Error adding creating the inscriptions table in the DB! ${err}`)
  }
})

const ponenciesSql = `CREATE TABLE IF NOT EXISTS ponencies (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL,
    description_short TEXT NOT NULL,
    description_long TEXT DEFAULT NULL,
    duration INTEGER NOT NULL,
    speaker TEXT NOT NULL,
    email TEXT NOT NULL,
    organization TEXT DEFAULT NULL,
    observations TEXT DEFAULT NULL,
    token TEXT
  )`

db.run(ponenciesSql, (err) => {
  if (err) {
    console.log(`Error adding creating the ponencies table in the DB! ${err}`)
  } else {
    console.log('Sucessfully created the two tables!')
  }
})
