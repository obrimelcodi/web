// Requires
const sqlite3 = require('sqlite3')

// Connect to the DB
const db = new sqlite3.Database('jornades.db', (err) => {
  if (err) {
    console.log(`Couldn't connect to the DB! ${err}`)
  } else {
    console.log('Successfully connected to the DB!')
  }
})

module.exports = db
