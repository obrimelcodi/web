// Requires
const nodemailer = require('nodemailer')
const account = require('../static/emailAccount.json')

// Email account setup
const transporter = nodemailer.createTransport({
  host: account.host,
  port: 465,
  secure: true,
  auth: {
    user: account.address,
    pass: account.password
  }
})

// Send the mail
const sendMail = (destinationEmail, emailSubject, emailBody) => {
  const mailOptions = {
    from: `"${account.name}" <${account.address}>`,
    to: destinationEmail,
    subject: emailSubject,
    html: emailBody
  }

  return transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      return [false, 'There was an error sending your email! ' + err]
    } else {
      return [true, 'Sucessfully sent your email!']
    }
  })
}

module.exports = sendMail
