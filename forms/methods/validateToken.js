// Requires
const db = require('./db')
const sendMail = require('./sendMail')
const account = require('../static/emailAccount.json')

// Validate Token
const validateToken = (tableName, token) => {
  return new Promise((resolve, reject) => {
    db.get(`SELECT * FROM ${tableName} WHERE token = ?`, [token], (err, row) => {
      if (err) {
        reject(err)
      } else {
        if (row === undefined) {
          reject("El token no s'ha trobat. Assegura't que l'has introduït correctament. En cas de ser així, potser ja s'ha confirmat")
        } else {
          db.run(`UPDATE ${tableName} SET token = NULL WHERE token = ?`, [token], (err) => {
            if (err) {
              reject(err)
            } else {
              var data = ''
              for (var column in row) {
                if (row.hasOwnProperty(column) && column !== 'id' && column !== 'token') {
                  data += `<li>${column.charAt(0).toUpperCase() + column.slice(1)}: ${row[column]}</li>`
                }
              }
              console.log(data)
              sendMail(row['email'], `Verificació a ${tableName} realitzada`, `<p>La teva sol·licitud s'ha verificat correctament!</p><ul>${data}</ul>`)
              sendMail(account.contactAddress, `Verificació a ${tableName} realitzada`, String(JSON.stringify(row, null, 4)))
              resolve("<p>S'ha confirmat la inscripció correctament!</p><a href='https://obrimelcodi.cat'>Tornar a Obrim el Codi</a>")
            }
          })
        }
      }
    })
  })
}

module.exports = validateToken
