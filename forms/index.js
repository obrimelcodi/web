// Requires
const express = require('express')
const bodyParser = require('body-parser')
const validator = require('express-validator')

// Routes requirements
const inscripcio = require('./routes/inscripcio')
const ponencia = require('./routes/ponencia')
const validate = require('./routes/validate')

// Initialize the ExpressJS app
const app = express()

// Middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(validator())

// Routes
app.use('/forms/inscripcio', inscripcio)
app.use('/forms/ponencia', ponencia)
app.use('/forms/validate', validate)

// Run the app
app.listen('3000', () => {
  console.log('App listening on port 3000!')
})
