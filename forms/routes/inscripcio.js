// Requires
const express = require('express')
const router = express.Router()
const uuid = require('uuid/v4')
const db = require('../methods/db')
const sendMail = require('../methods/sendMail')
const settings = require('../static/settings')

// POST: /forms/inscripcio; Add a new inscription
router.post('/', (req, res) => {
  req.checkBody('name', 'Siusplau, introdueix el teu nom!').isString().notEmpty()
  req.checkBody('surnames', 'Siusplau, introdueix els teus cognoms!').isString().notEmpty()
  req.checkBody('email', 'Siusplau, introdueix una adreça de correu electrònic vàlida!').isEmail()

  if (req.body.emailUpdates === undefined) {
    req.body.emailUpdates = '0'
  } else if (req.body.emailUpdates !== '1') {
    res.send('Siusplau, digues si vols rebre actualitzacions a través del correu electrònic!')
  }

  if (req.body.lunch === undefined) {
    req.body.lunch = '0'
  } else if (req.body.lunch !== '1') {
    res.send('Siusplau, digues si assistiràs o no al dinar!')
  }

  const err = req.validationErrors()
  if (err) {
    var errorList = ''
    err.forEach(error => {
      errorList += `<li>${error['msg']}</li>`
    })

    res.send(`<ul>${errorList}</ul><br><a href="#" onclick="window.history.back()">Tornar al formulari</a>`)
  } else {
    const token = uuid()
    const sql = `INSERT INTO inscripcions
    (name, surnames, email, organization, email_updates, lunch, token)
    VALUES (?, ?, ?, ?, ?, ?, ?)`
    db.run(sql, [
      req.body.name,
      req.body.surnames,
      req.body.email,
      req.body.organization,
      Number(req.body.emailUpdates),
      Number(req.body.lunch),
      token
    ], (err) => {
      if (err) {
        res.send(`<p>Hi ha hagut un error guardant la inscripció! ${err}</p>`)
      } else {
        sendMail(req.body.email, 'Inscripció a les Jornades Obrim el Codi', `<p>Hola, ${req.body.name} ${req.body.surnames}. Siusplau, confirma la teva assistència utilitzant el següent enllaç:<p>
<a href="${settings.protocol}://${settings.domainName}/validate/inscripcions/${token}">${settings.protocol}://${settings.domainName}/validate/inscripcions/${token}</a>`)
        res.send("<p>S'ha creat correctament la inscripció! Vés al teu correu electrònic per a validar-la!</p>")
      }
    })
  }
})

module.exports = router
