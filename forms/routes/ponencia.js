// Requires
const express = require('express')
const router = express.Router()
const uuid = require('uuid/v4')
const db = require('../methods/db')
const sendMail = require('../methods/sendMail')
const settings = require('../static/settings')

// POST: /forms/ponencia; Add a new speech request
router.post('/', (req, res) => {
  req.checkBody('title', 'Siusplau, introdueix el títol de la ponència!').isString().notEmpty()
  req.checkBody('descriptionShort', 'Siusplau, introdueix una descripció curta de la ponència (màxim 250 caràcters)!').isString().isLength({
    max: 250
  }).notEmpty()
  req.checkBody('duration', 'Siusplau introdueix una duració correcta (o bé 5 [minuts] o bé 15 [minuts])!').matches(/\b(?:5|15)\b/)
  req.checkBody('speaker', 'Siusplau, introdueix el nom de la persona que farà la ponència!').isString().notEmpty()
  req.checkBody('email', 'Siusplau, introdueix una adreça de correu electrònic vàlida!').isEmail()

  const err = req.validationErrors()
  if (err) {
    var errorList = ''
    err.forEach(error => {
      errorList += `<li>${error['msg']}</li>`
    })

    res.send(`<ul>${errorList}</ul><br><a href="#" onclick="window.history.back()">Tornar al formulari</a>`)
  } else {
    const token = uuid()
    const sql = `INSERT INTO ponencies
    (title, description_short, description_long, duration, speaker, email, organization, observations, token)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`
    db.run(sql, [
      req.body.title,
      req.body.descriptionShort,
      req.body.descriptionLong,
      Number(req.body.duration),
      req.body.speaker,
      req.body.email,
      req.body.organization,
      req.body.observations,
      token
    ], (err) => {
      if (err) {
        res.send(`<p>Hi ha hagut un error guardant la ponència! ${err}</p>`)
      } else {
        sendMail(req.body.email, 'Ponència a les Jornades Obrim el Codi', `<p>Hola, ${req.body.speaker}. Siusplau, confirma la teva ponència utilitzant el següent enllaç:</p>
<a href="${settings.protocol}://${settings.domainName}/validate/ponencies/${token}">${settings.protocol}://${settings.domainName}/validate/ponencies/${token}</a>`)
        res.send(`<p>S'ha creat correctament la ponència! Vés al teu correu electrònic per a validar-la!</p>`)
      }
    })
  }
})

module.exports = router
